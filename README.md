# Docmans-smart-contracts

This repository contains smart contracts for Docmans - a distributed document
management system.

## Prerequisites

---

To follow this tutorial, you will need the following:

- Node
- Npm

To follow

## Dependencies install

### _node & npm_

<https://github.com/nvm-sh/nvm>

To install and use node LTS version:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

nvm install 10.16.3 && nvm use 10.16.3
```

### _ganache-cli_

https://github.com/trufflesuite/ganache-cli#installation

## _Environment desc_

```bash
node -v
v10.16.0

npm -v
6.9.0

uname -a
Linux user-name 5.0.0-25-generic #26~18.04.1-Ubuntu SMP Date x86_64 x86_64 x86_64 GNU/Linux
```

## _Install_

---

```bash
npm i
```

## _Tests_

---

To run tests:

1. Start development blockchain with `ganache-cli`

2. Start tests with `npm run test`

This command starts a docker based _ganache-cli_ instance and invokes tests via truffle framework

## _Linting and Formatting_

---

To run solidity and markdown linters

```bash
npm run lint
```

To run code formatter

```bash
npm run format
```

## _Smart Contracts deployment_

---

**TBD**
