pragma solidity ^0.5.10;
pragma experimental ABIEncoderV2;

contract FileToken {
    constructor() public {
    }

    enum PermissionType {
        Read,
        Write
    }

    struct Permissions {
        address owner;
        address[] allowedUsers;
        mapping (address => PermissionType) permissions;
    }

    struct File {
        Permissions permissions;
    }

    struct Directory {
        Permissions permissions;

        string[] directoriesNames;
        string[] filesNames;

        mapping (string=>Directory) directories;
        mapping (string=>bool) directoryExists;

        mapping (string=>File) files;
        mapping (string=>bool) fileExists;
    }

    mapping (address=>Directory) private rootDirectoryByUserId;
    mapping (address=>bool) private rootDirectoryRegistered;

    mapping (address=>string) public addressToUsername;
    mapping (address=>Directory) private sharedDirectories;

    function registerRootDirectory(string memory username)
        public
    {
        require(!rootDirectoryRegistered[msg.sender], "user is already registered");
        rootDirectoryByUserId[msg.sender].permissions.owner = msg.sender;
        sharedDirectories[msg.sender].permissions.owner = msg.sender;
        addressToUsername[msg.sender] = username;
        rootDirectoryRegistered[msg.sender] = true;
    }

    function _addChild(
        Directory storage root,
        string memory childName,
        string[] memory parent,
        uint pathLength,
        bool isFile
    )
        private
        returns(Directory memory)
    {
        if (pathLength == 0) {
            if (isFile) {
                require(!root.directoryExists[childName], "directory with the same name exists");
                Permissions memory permissions;
                permissions.owner = msg.sender;
                root.files[childName] = File(permissions);
                root.fileExists[childName] = true;
                root.filesNames.push(childName);
            } else {
                require(!root.fileExists[childName], "file with the same name exists");
                string[] memory names;
                Permissions memory permissions;
                permissions.owner = msg.sender;
                root.directories[childName] = Directory(permissions, names, names);
                root.directoryExists[childName] = true;
                root.directoriesNames.push(childName);
            }

            return root;
        }

        string memory nextRootName = parent[0];
        require(root.directoryExists[nextRootName], "no such directory");
        Directory storage nextRoot = root.directories[nextRootName];
        for (uint i = 0; i < pathLength - 1; i++) {
            parent[i] = parent[i + 1];
        }

        root.directories[nextRootName] = _addChild(nextRoot, childName, parent, pathLength - 1, isFile);
        return root;
    }

    function addChild(
        string memory childName,
        string[] memory parent,
        bool isFile
    )
        public
    {
        require(rootDirectoryRegistered[msg.sender], "no such user");
        rootDirectoryByUserId[msg.sender] = _addChild(
            rootDirectoryByUserId[msg.sender],
            childName,
            parent,
            parent.length,
            isFile
        );
    }

    function _addPermission(
        Directory storage root,
        Directory storage sharedRoot,
        address user,
        PermissionType permissionType,
        string[] memory parent,
        uint pathLength
    )
        private
        returns (Directory memory, Directory memory)
    {
        if (pathLength == 1) {
            string memory targetName = parent[0];

            if (root.fileExists[targetName]) {
                Permissions storage permissions = root.files[targetName].permissions;

                bool inAllowedUsers = false;
                for (uint i = 0; i < permissions.allowedUsers.length; i++) {
                    inAllowedUsers = inAllowedUsers || (permissions.allowedUsers[i] == user);
                }

                if (permissions.permissions[user] == permissionType && inAllowedUsers) {
                    return (root, sharedRoot);
                }

                if (!inAllowedUsers) {
                    permissions.allowedUsers.push(user);
                }

                permissions.permissions[user] = permissionType;

                if (!sharedRoot.fileExists[targetName]) {
                    sharedRoot.fileExists[targetName] = true;
                    sharedRoot.filesNames.push(targetName);
                }

                root.files[targetName].permissions = permissions;
                sharedRoot.files[targetName].permissions = permissions;
            } else if (root.directoryExists[targetName]) {
                Permissions storage permissions = root.directories[targetName].permissions;

                bool inAllowedUsers = false;
                for (uint i = 0; i < permissions.allowedUsers.length; i++) {
                    inAllowedUsers = inAllowedUsers || (permissions.allowedUsers[i] == user);
                }

                if (permissions.permissions[user] == permissionType && inAllowedUsers) {
                    return (root, sharedRoot);
                }

                if (!inAllowedUsers) {
                    permissions.allowedUsers.push(user);
                }

                permissions.permissions[user] = permissionType;

                if (!sharedRoot.directoryExists[targetName]) {
                    sharedRoot.directoryExists[targetName] = true;
                    sharedRoot.directoriesNames.push(targetName);
                }

                root.directories[targetName].permissions = permissions;
                sharedRoot.directories[targetName].permissions = permissions;

                for (uint i = 0; i < root.directories[targetName].filesNames.length; i++) {
                    string[] memory filePath = new string[](1);
                    filePath[0] = root.directories[targetName].filesNames[i];
                    (root.directories[targetName], sharedRoot.directories[targetName]) =
                        _addPermission(
                            root.directories[targetName],
                            sharedRoot.directories[targetName],
                            user,
                            permissionType,
                            filePath,
                            1
                        );
                }

                for (uint i = 0; i < root.directories[targetName].directoriesNames.length; i++) {
                    string[] memory dirPath = new string[](1);
                    dirPath[0] = root.directories[targetName].directoriesNames[i];
                    (root.directories[targetName], sharedRoot.directories[targetName]) =
                        _addPermission(
                            root.directories[targetName],
                            sharedRoot.directories[targetName],
                            user,
                            permissionType,
                            dirPath,
                            1
                        );
                }
            } else {
                revert("No such node");
            }

            return (root, sharedRoot);
        }

        string memory nextRootName = parent[0];
        require(root.directoryExists[nextRootName], "no such directory");

        if (!sharedRoot.directoryExists[nextRootName]) {
            sharedRoot.directories[nextRootName].permissions.owner = msg.sender;
            sharedRoot.directoryExists[nextRootName] = true;
            sharedRoot.directoriesNames.push(nextRootName);
        }

        Directory storage nextRoot = root.directories[nextRootName];
        Directory storage nextSharedRoot = sharedRoot.directories[nextRootName];
        for (uint i = 0; i < pathLength - 1; i++) {
            parent[i] = parent[i + 1];
        }

        (root.directories[nextRootName], sharedRoot.directories[nextRootName]) =
            _addPermission(nextRoot, nextSharedRoot, user, permissionType, parent, pathLength - 1);
        return (root, sharedRoot);
    }

    function addPermission(string[] memory nodePath, address user, PermissionType permissionType)
        public
    {
        require(rootDirectoryRegistered[msg.sender], "no such user");

        // Create `/shared/<username>` if do not exist
        if (!sharedDirectories[user].directoryExists[addressToUsername[msg.sender]]) {
            sharedDirectories[user].directories[addressToUsername[msg.sender]].permissions.owner = msg.sender;
            sharedDirectories[user].directoryExists[addressToUsername[msg.sender]] = true;
            sharedDirectories[user].directoriesNames.push(addressToUsername[msg.sender]);
        }

        (rootDirectoryByUserId[msg.sender], sharedDirectories[user].directories[addressToUsername[msg.sender]]) = _addPermission(
            rootDirectoryByUserId[msg.sender],
            sharedDirectories[user].directories[addressToUsername[msg.sender]],
            user,
            permissionType,
            nodePath,
            nodePath.length
        );
    }

    function _getPermissions(
        Directory storage root,
        string[] memory nodePath,
        uint pathLength
    )
        private
        view
        returns(address owner, address[] memory user, PermissionType[] memory permissionType)
    {
        if (pathLength == 1) {
            if (root.fileExists[nodePath[0]]) {
                uint permissionsSize = root.files[nodePath[0]].permissions.allowedUsers.length;
                permissionType = new PermissionType[](permissionsSize);

                for (uint i = 0; i < permissionsSize; i++) {
                    address user = root.files[nodePath[0]].permissions.allowedUsers[i];
                    permissionType[i] = root.files[nodePath[0]].permissions.permissions[user];
                }

                return (root.files[nodePath[0]].permissions.owner, root.files[nodePath[0]].permissions.allowedUsers, permissionType);
            } else if (root.directoryExists[nodePath[0]]) {
                uint permissionsSize = root.directories[nodePath[0]].permissions.allowedUsers.length;
                permissionType = new PermissionType[](permissionsSize);

                for (uint i = 0; i < permissionsSize; i++) {
                    address user = root.directories[nodePath[0]].permissions.allowedUsers[i];
                    permissionType[i] = root.directories[nodePath[0]].permissions.permissions[user];
                }

                return (root.directories[nodePath[0]].permissions.owner, root.directories[nodePath[0]].permissions.allowedUsers, permissionType);
            } else {
                revert("no such file or directory");
            }
        }

        string memory nextRootName = nodePath[0];
        require(root.directoryExists[nextRootName], "no such directory");
        Directory storage nextRoot = root.directories[nextRootName];
        for (uint i = 0; i < pathLength - 1; i++) {
            nodePath[i] = nodePath[i + 1];
        }

        return _getPermissions(nextRoot, nodePath, pathLength - 1);
    }

    function getPermissions(
        address rootOwner,
        string[] memory nodePath
    )
        public
        view
        returns(address owner, address[] memory user, PermissionType[] memory permissionType)
    {
        require(rootDirectoryRegistered[rootOwner], "no such user");
        return _getPermissions(rootDirectoryByUserId[rootOwner], nodePath, nodePath.length);
    }

    function _getSharedDirectories(
        Directory storage root,
        string[] memory nodePath,
        uint pathLength
    )
        private
        view
        returns (string[] memory dirs, string[] memory files)
    {
        if (pathLength == 0) {
            return (root.directoriesNames, root.filesNames);
        }

        string memory nextRootName = nodePath[0];
        require(root.directoryExists[nextRootName], "no such directory");
        Directory storage nextRoot = root.directories[nextRootName];
        for (uint i = 0; i < pathLength - 1; i++) {
            nodePath[i] = nodePath[i + 1];
        }

        return _getSharedDirectories(nextRoot, nodePath, pathLength - 1);
    }

    function getSharedDirectories(address user, string[] memory nodePath)
        public
        view
        returns (string[] memory dirs, string[] memory files)
    {
        require(rootDirectoryRegistered[user], "no such user");
        return _getSharedDirectories(sharedDirectories[user], nodePath, nodePath.length);
    }
}
